/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id11965252.domain;

/**
 *
 * Pen model
 */
public class Pen extends Good{
    
    private enum Thickness{
        FINE("F"),
        MEDIUM("M"),
        BOLD("B");
        
        private String description;
        
        private Thickness(String description){
            this.description = description;
        }
        
        @Override
        public String toString(){
            return this.description;
        }
    }
    
    Thickness thickness;
        
}

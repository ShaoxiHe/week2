/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id11965252.domain;

import java.math.BigDecimal;

/**
 *
 * Good class
 * Interface Model class of each Good
 */
public abstract class Good {
    
    BigDecimal price;
    String name;
    String code;
    String brand;
}
